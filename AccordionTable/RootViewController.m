//
//  RootViewController.m
//  AccordionTable
//
//  Created by Alex on 3/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RootViewController.h"

@implementation RootViewController

@synthesize movieTitles, years, headers;

- (void)viewDidLoad
{
    //Set the title of the table
    self.navigationItem.title = @"List of Movies";
	
	//Set up a dummy varible to -1, this will help us see what whas the last header section that was clicked
	lastSectionClicked = -1;
    
    //path to the property list file
	NSString *path = [[NSBundle mainBundle] pathForResource:@"movies"
													 ofType:@"plist"];
	//load the list into the dictionary
	NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:path];
	
	//save the dictionary object to the property
	self.movieTitles = dic; 
	[dic release];
	
	//get all the keys in the dictionary object and sort them
	NSArray *array = [[self.movieTitles allKeys] sortedArrayUsingSelector:@selector(compare:)];
	
	//save the keys in the years property
	self.years = array;
	
	//Create the headers Mutable Array
	headers = [[NSMutableArray alloc]init];
	
	//Populate the array with dummy information so I can use insertObjectAtIndex
	//I found that I cant just add items at an index, so I needed to populate the array with dummy data.
	[headers addObject:@"temp"];
	[headers addObject:@"temp"];
	[headers addObject:@"temp"];
	[headers addObject:@"temp"];
	[headers addObject:@"temp"];

    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}


 // Override to allow orientations other than the default portrait orientation.
 // I did not implement this.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
 

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.years count];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	//Check to see if the header subsection is expanded.
	//I had to make a check here because when I deleted the rows, the program will redraw the rows.
	if ([self findIfExpanded:section] ) {
	
	
    //check the current year based on the section index 
    NSString *year = [self.years objectAtIndex:section];
    
    //returns the movies in that year as an array
    NSArray *movieSection = [self.movieTitles objectForKey:year];
    
    //return the number of movies for that year as the number of rows in that 
    // section 
    return [movieSection count];
		
	}
	
	else {
		//return 0 because the rows have been "rolled up"
		return 0;
	}

}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    // Configure the cell.
    NSString *year = [self.years objectAtIndex:[indexPath section]];
    
    //get the list of movies for that year 
    NSArray *movieSection = [self.movieTitles objectForKey:year];
    
    //get the particular movie based on that row
    cell.textLabel.text = [movieSection objectAtIndex:[indexPath row]];
	
	//Disable the seleciton of the cell, since nothing is going to happen
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

//Here is where I set the header to the headerView that I made. 
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

	//Check to see if the headers array has been populated arlready. If it has not been, then add a new headerView to the array
	if ([[headers objectAtIndex:section] isKindOfClass:[NSString class]]) {

		//Create the headerView
		headerView * headerInSection = [headerView headerViewWithTitle:[self.years objectAtIndex:section]];
    
		//Set the buttons action to the toggleSection method. We will only get the sender object
		[headerInSection.button addTarget:self action:@selector(toggleSection:) forControlEvents:UIControlEventTouchUpInside];
    
		//A real trick here, I can set the tag of the button to the section that it represents. This way I know what header was selected
		headerInSection.button.tag = section;
	
		//Replace the text object in the array with the headerView
		[headers replaceObjectAtIndex:section withObject:headerInSection];
    

		return headerInSection;
	}
	
	else {
		//Return the current headerView that is in the array
		return [headers objectAtIndex:section];
	}
}

//This method will be called whenever a button from the headers is pressed
//Key to note, that we only get the sender, witch is the button. I have designed the button with a tag that correspondes to its section
- (void)toggleSection : (id) sender {
	
	//Cast the sender into a button
	UIButton *selected = (UIButton*) sender;
	
	//Get the tag id of the button, this is also the section that the button was a header for.
	int  tid = [selected tag];
	
	//Toggle the header that was selected
	[self toggle:[self findIfExpanded:tid] section:tid];
	
	//A little logic saved some time.
	
	//If this is the first item that was selected, then update the lastSectionClicked
	if (lastSectionClicked == -1) {
		lastSectionClicked = tid;

	}
	
	// if the same header was clicked on twice, then reset the lastSectionClicked as we have already toggled the header
	else if(lastSectionClicked == tid)	{

		lastSectionClicked = -1;

	}
	//Base case, after we have already togled open another header, we need to close the last header that was opened
	else {
		[self toggle:YES section:lastSectionClicked];
		lastSectionClicked = tid;
	}
}

//This method will toggle the opened or closed state of the header and its contents
- (void)toggle:(BOOL)isExpanded section:(NSInteger)section {

	//Find all the subsections in the table
	NSArray *paths = [self indexPathsInSection:section];
	
	//if we are to close the header and its section
	if ( isExpanded ) {
		
		//Call the invert method on the headerView object: change the color, change the icon, set the expanded bool to NO
		[[headers objectAtIndex:section] invert];
		
		//This is where we will remove the items from the table view. We will use an animation. Note that since we are changing only the view here
		//the model is saved.
		[self.tableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
		
	}
	else {
		
		//Call the invert method on the headerView object: change the color, change the icon, set the expanded bool to YES
		[[headers objectAtIndex:section] invert];
		
		//This is where we add the items to a header to make it look like we are expanding the header. Note, since we are adding items only to the view
		//the model that holds the data is not changed.
		[self.tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];

	}
}

//Return the items under a header 
- (NSArray*)indexPathsInSection:(NSInteger)section {
	NSMutableArray *paths = [NSMutableArray array];
	NSInteger row;
	
	for ( row = 0; row < [self numberOfRowsInSection:section]; row++ ) {
		[paths addObject:[NSIndexPath indexPathForRow:row inSection:section]];
	}
	
	return [NSArray arrayWithArray:paths];
}

//return the number of rows in a section.
- (NSInteger)numberOfRowsInSection:(NSInteger)section {
		return [self findRowsInSection:section];
}

//A layout issue
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 44;
}

-(NSInteger) findRowsInSection:(NSInteger) section{
    //check the current year based on the section index
    NSString *year = [self.years objectAtIndex:section];
    
    //returns the movies in that year as an array
    NSArray *movieSection = [self.movieTitles objectForKey:year];
    
    //return the number of movies for that year as the number of rows in that 
    // section
    return [movieSection count];
}

//Here we check to see if the section has been expanded or not.
-(BOOL) findIfExpanded:(NSInteger) section{
    
	headerView *temp = [headers objectAtIndex:section];
	
	return temp.expanded;
	 
	 }

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];

    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)dealloc
{
    [movieTitles release];
    [years release];
	[headers release];
    [super dealloc];
}

@end
