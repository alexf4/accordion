//
//  RootViewController.h
//  AccordionTable
//
//  Created by Alex on 3/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "headerView.h"

//This class will implement the UITableViewController
@interface RootViewController : UITableViewController {
	//A dictionary that will hold the movie data
    NSDictionary *movieTitles; 
    
	//An array that will hold the header text
	NSArray *years;
	
	//A mutable array that will hold the headerView objects
    NSMutableArray * headers;
	
	//A reference to the last header that was clicked. This saves time by doing only one toggle to reset the table,
	//instead of checking each header to see if it needs to be closed.
	// It is used in - (void)toggleSection : (id) sender
	int lastSectionClicked;
}


@property (nonatomic, retain) NSDictionary *movieTitles; 
@property (nonatomic, retain) NSArray *years;
@property (nonatomic, retain) NSMutableArray * headers;

//See RootViewController.m for details
- (NSArray*)indexPathsInSection:(NSInteger)section;

- (NSInteger)numberOfRowsInSection:(NSInteger)section;

-(BOOL) findIfExpanded:(NSInteger) section;

-(NSInteger) findRowsInSection:(NSInteger) section;

- (void)toggle:(BOOL)isExpanded section:(NSInteger)section;

- (void)toggleSection : (id) sender;

@end
