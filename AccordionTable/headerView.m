//
//  headerView.m
//  AccordionTable
//
//  Created by Alex on 3/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "headerView.h"


@implementation headerView


@synthesize mainView, button, label, expanded, chevron;

//A method that will allow users to create a new headerView and set its title
+ (id)headerViewWithTitle:(NSString*)title {
	
	headerView *hView = [[headerView alloc] init];
	[hView.label setText:title];
	return [hView autorelease];
}


//This method will be called when we need to close or open a sublist. 
-(void) invert{
		expanded = !expanded;
	
		if (expanded) {
			//change the color of the lable color
			label.backgroundColor = [UIColor purpleColor];
			//change the chevron
			[chevron setImage:[UIImage imageNamed:@"arrow-blue-rounded-up.jpg"]];
		}
		else {
			//change the color of the lable color
			label.backgroundColor = [UIColor blueColor];
			//change the chevron
			[chevron setImage:[UIImage imageNamed:@"arrow-blue-rounded-down.jpg"]];
		}

}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) init {
	self = [super initWithFrame:CGRectMake(0., 0., 320., 44.)];
	if (self != nil) {
		[[NSBundle mainBundle] loadNibNamed:@"headerView" owner:self options:nil];
		
        expanded = NO;
		
		[chevron setImage:[UIImage imageNamed:@"arrow-blue-rounded-down.jpg"]];
        
		[self addSubview:mainView];
	}
	
	return self;
}

- (void)dealloc
{
    [mainView release];
    [button release];
    [label release];
	[chevron release];
    [super dealloc];
}

@end
