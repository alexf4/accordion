//
//  headerView.h
//  AccordionTable
//
//  Created by Alex on 3/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


//Here we will subclass the UIView
@interface headerView : UIView {
    //The main view
	IBOutlet UIView *mainView;
    
    //The button that will allow the user to tap the header and get a response from it
	IBOutlet UIButton *button;
    
    //The text that will show on the header
	IBOutlet UILabel *label;
    
	//This will mark if the contents under this header are expanded
    BOOL expanded;
	
	//A simple image view that is oriented according to the expansion of the sub elements under the header.
	IBOutlet UIImageView * chevron;
    
}

//I will get ownership of this class varibles and release them in the dealloc. 
@property (retain, nonatomic) UIView *mainView;
@property (retain , nonatomic) UIButton *button;
@property (retain, nonatomic) UILabel *label;
@property (retain, nonatomic) UIImageView * chevron;

@property (assign, nonatomic) BOOL expanded;

//A method that will allow me to create a new headerView with the text being set to the input.
+ (id)headerViewWithTitle:(NSString*)title;

//This method will invert the header from display to hide mode
-(void) invert;

@end
